from django import forms

from .models import Calculation, Post


class CalculationForm(forms.ModelForm):
    class Meta:
        model = Calculation
        fields = ["name", "input_arguments"]
        widgets = {
            "input_arguments": forms.Textarea(attrs={"rows": 3}),
        }

    def set_calculation_engine(self, engine):
        self.engine = engine

    def save(self, commit=True):
        calculation = super().save(commit=False)
        # Perform the calculation here using the input arguments
        # and populate the output_responses field
        input_arguments = calculation.input_arguments.split(",")
        inputs = map(float, input_arguments)
        calculation.output_responses = self.engine.process(*inputs)
        if commit:
            calculation.save()
        return calculation


def perform_calculation(input_arguments):
    # Perform the calculation logic here
    # and return the calculated output
    # This is just a sample implementation
    # Replace it with your actual calculation logic
    result = eval(input_arguments)
    return str(result)


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ("title", "content")
        widgets = {
            "title": forms.TextInput(attrs={"class": "form-control"}),
            "content": forms.Textarea(attrs={"class": "form-control"}),
        }
