from rich import print


class OperationLogger:
    def log(self, operation, args, results):
        name = operation.__class__.__name__

        msg = (
            f"[green]Operation:[/green] {name} "
            f"Arguments: [green]{args}[/green] Results: {results}"
        )
        print(msg)
