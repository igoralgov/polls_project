from django.http import HttpResponse
from django.shortcuts import redirect, render

from polls_project.factories import CalculationFactory

from .forms import CalculationForm, PostForm
from .models import Calculation, Post


def create_calculation_view(request, post_id):
    post = Post.objects.get(id=post_id)

    if request.method == "POST":
        form = CalculationForm(request.POST)
        if form.is_valid():
            calculation_name = form.cleaned_data["name"]
            calculation = CalculationFactory().get(calculation_name)
            form.set_calculation_engine(calculation)
            calculation = form.save(commit=False)
            calculation.post = post
            calculation.save()
            return redirect("post_list")
    else:
        form = CalculationForm()

    return render(request, "create_calculation.html", {"form": form, "post": post})


def index(request):
    return HttpResponse("Hello, world.")


def post_list(request):
    posts = Post.objects.all()
    return render(request, "post_list.html", {"posts": posts})


def post_create(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("post_list")
    else:
        form = PostForm()
    return render(request, "post_create.html", {"form": form})
