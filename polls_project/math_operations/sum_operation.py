from .operations import Operation


class SumOperation(Operation):
    def compute(self, first_operand, second_operand):
        return self.sum(first_operand, second_operand)

    def sum(self, a, b):
        return a + b


class DivOperation:
    def __init__(self):
        pass

    def div(self, a, b):
        return a / b
