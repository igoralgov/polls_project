from polls_project.math_operations.operations import Operation


class ModOperation(Operation):
    def __init__(self):
        pass

    def compute(self, first_operand, second_operand):
        return self.mod(first_operand, second_operand)

    def mod(self, a, b):
        return a % b
