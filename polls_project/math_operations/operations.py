from abc import ABC, abstractmethod


class Operation(ABC):
    @abstractmethod
    def compute(self, *args):
        pass
