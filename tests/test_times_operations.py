from polls_project.factories import CalculationFactory
from polls_project.math_operations.times_operation import TimesOperation


def test_times_1_2_should_be_2():
    # given
    a = 1
    b = 2
    expected = 2
    times_object = TimesOperation()
    # when
    result = times_object.times(a, b)
    # then
    assert result == expected


def test_operation_equation_process_factory():
    # given
    a = 2
    b = 2
    expected = 4
    processor = CalculationFactory().get("*")
    # when
    result = processor.process(a, b)
    # then
    assert result == expected
