from polls_project.math_operations.sub_operation import SubOperation


def test_sub_5_2_should_be_3():
    # given
    a = 5
    b = 2
    expected = 3
    sub_object = SubOperation()
    # when
    result = sub_object.sub(a, b)
    # then
    assert result == expected
